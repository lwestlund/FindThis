# FindThis

[![Build and Tests](https://github.com/lovewestlundgotby/FindThis/actions/workflows/build_and_test.yml/badge.svg)](https://github.com/lovewestlundgotby/FindThis/actions/)
[![codecov](https://codecov.io/gh/lovewestlundgotby/FindThis/branch/develop/graph/badge.svg)](https://codecov.io/gh/cpp-best-practices/cpp_starter_project)

FindThis is a program that finds things for you. It can help you find either that
file somewhere in your filesystem, that file containing some text that you are
looking for, or where in your file you wrote that thing. FindThis will do this
for you, as long as you can tell it what you are looking for in a regex pattern.

## Table of contents

- [Building](#building)
- [How to use](#how-to-use)
- [Implementation details](#implementation-details)

FindThis is a tribute to the tools that I use everyday in my work or other hobby
projects, namely [ripgrep (`rg`)](https://github.com/BurntSushi/ripgrep) and
[fd](https://github.com/sharkdp/fd), and is intended to be something like a love
child between the two. It does not have all the fancy features of the previous
mentions, but is more about the challenge of doing what they do myself.

Besides being a command line tool like `rg` and `fd`, FindThis (`ft` on the
command line) is also available as a C++ library (`libft`) that can power
searches for you in *your* program. This is probably most usable in for example
editors, where with `libft` you can do powerful regular expression searches
from within your application, without the need to run a shell command and parse
the printed output.

## Building

Building requires CMake 3.20 and a C++ compiler with C++20 support. At the
moment of writing this, building has only been verified on Linux using GCC
11.1.0. There are some C++20 features used that are not properly supported by
clang 14.0.0, and builds on MacOS and Windows have not been done because of a
dependency on Boost that I have not bothered to handle.

So, if you are on Linux and you have an at least fairly new version of GCC and
CMake, you should hopefully be able to build it. To help with this, there is a
build generator helper in [`tools/prepare_build.py`](./tools/prepare_build.py)
that wraps CMake. The use of it is of course optional, but is meant to help
working with different build configurations. The minimal use case is

``` sh
./tools/prepare_build.py --build-path <path/to/build/dir>
```

and this will configure a `Release` build with the first native C++ compiler that
CMake can find and `Ninja` as generator. The pure CMake equivalent is

``` sh
cmake -DCMAKE_BUILD_TYPE=Release -G Ninja -B <path/to/build/dir> -S <path/to/repo/root>
```

With the build all setup, all you need to do is to go to the build directory and
build things! There are four major targets defined in CMake

- `ft`, the `ft` executable that you run from the command line
- `libft`, the (at the moment static) library that you can use in your own
  application
- `tests_component`, a target that build all the unit tests, to run the tests,
  `ctest` can be used to execute them all or you can run individual test
  executables located in the build tree
- `coverage_report`, generating a coverage report using [lcov](http://ltp.sourceforge.net/coverage/lcov.php)
  from the defined unit tests

## How to use

### CLI

From the command line, `ft` is intended to be quick to use, with the shortest
command being

``` sh
ft PATTERN
```

to search for a `PATTERN` in files located in the current and any subdirectories.
The general usage, however, is

``` sh
ft [OPTIONS] PATTERN [PATH ...]
```

where you can specify any options, the pattern you are looking for and any paths
you want to look in. You can ask `ft` to describe how to use it and what options
there are using the `-h/--help` option and get the following output.

``` text
Usage:
  ft [OPTIONS] PATTERN [PATH ...]

Positional arguments:
  --pattern PATTERN         Search for pattern.
  --path PATH               Search path.

Options:
  -f [ --filename ]         Search for filenames matching PATTERN.
  -i [ --ignore-case ]      Search case insensitively.
  -s [ --case-sensitive ]   Search case sensitively, overrides --ignore-case.
  -t [ --type ] arg         Search for files with type, overrides --not-type.
  -T [ --not-type ] arg     Search for files not of type, overridden by --type.
  --type-list               Print list of supported file types.

Generic options:
  -h [ --help ]             Prints help message.
  -V [ --version ]          Prints version information.
```

### C++ library

This currently supports static linking only, and is easiest to do with
FindThis as e.g. a submodule integrated with CMake. If that is the case, all you
need to do is link with the target `libft` and you should be able to `#include`
the public [`ft/ft.hpp`](./ft/include/ft/ft.hpp) header, and start searching.

## Implementation details

The required filesystem traversal is done using the C++ STL `<filesystem>`
header, and the regular expression matching using the Boost `xpressive` module.
Originally, the filesystem was also powered by the Boost `filesystem` module
because I wanted to try out and work with Boost to see how it was. As the project
has evolved, and since the STL `<filesystem>` pretty much is the same as Boost
`filesystem`, it felt very natural to use the STL more since the same
functionality was available without the need for an external dependency.
