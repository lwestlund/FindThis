{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  nativeBuildInputs = [ pkg-config cmake ];
  buildInputs = [ boost172 ];
}
