if(FT_ENABLE_TESTING)
  enable_testing()

  add_custom_target(tests_component)

  # ~~~
  # ft_add_test
  #
  # Add a test executable to the project.
  # All added tests can be built using the `tests` target.
  #
  # Arguments:
  # NAME       name of the test
  # SOURCES    the sources that should be compiled as part of the test
  # INCLUDES   extra include paths needed by the test
  # LIBRARIES  libraries that the test should be linked with
  # ~~~
  function(ft_add_test)
    set(options)
    set(oneValueArgs NAME)
    set(multiValueArgs SOURCES INCLUDES LIBRARIES)
    cmake_parse_arguments(
      "TEST"
      "${options}"
      "${oneValueArgs}"
      "${multiValueArgs}"
      ${ARGN}
    )

    add_executable(${TEST_NAME} ${TEST_SOURCES})
    target_link_libraries(${TEST_NAME} PRIVATE ${TEST_LIBRARIES} doctest)
    target_include_directories(
      ${TEST_NAME} PRIVATE $<TARGET_PROPERTY:doctest,INTERFACE_INCLUDE_DIRECTORIES>
                           ${TEST_INCLUDES}
    )

    add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})

    add_dependencies(tests_component ${TEST_NAME})
  endfunction()
endif()
