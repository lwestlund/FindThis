if(CMAKE_BUILD_TYPE STREQUAL Coverage)
  find_program(lcov_program lcov)

  if(NOT lcov_program)
    message(FATAL_ERROR "lcov is required for computing code coverage")
  endif()

  set(CMAKE_CXX_FLAGS_COVERAGE_INIT CMAKE_CXX_FLAGS_DEBUG_INIT)

  set(_coverage_flags
      -O0 # Do not optimize.
      -g # Produce debugging information.
      --coverage # Compile and link code instrumented for coverage analysis.
  )
  string(
    JOIN
    " "
    CMAKE_CXX_FLAGS_COVERAGE
    ${_coverage_flags}
  )

  include(ProcessorCount)
  ProcessorCount(_num_cpus)
  # cmake-format: off
  if(NOT _num_cpus EQUAL 0)
  # cmake-format: on
    set(_num_cpus 1)
  endif()

  add_custom_command(
    OUTPUT dummy_coverage.info # Set the output to a dummy file so that the command is always run.
    COMMAND ctest --output-on-failure --parallel ${_num_cpus}
    COMMAND ${lcov_program} --capture --directory . --exclude '/usr/*' --exclude '*/third_party/*'
            --output-file coverage.info
    COMMAND ${lcov_program} --remove coverage.info '*/test/*' --output-file coverage.info
    DEPENDS tests_component
    COMMENT "Generating coverage info"
  )

  add_custom_target(coverage DEPENDS dummy_coverage.info)

  find_program(genhtml_program genhtml)
  if(NOT genhtml_program)
    message(WARNING "genhtml is required for generating a browsable code coverage report")
  else()
    add_custom_target(
      coverage_report
      COMMAND ${genhtml_program} coverage.info --output-directory coverage_report
      DEPENDS dummy_coverage.info
      COMMENT "Generating coverage report"
    )
  endif()
endif()
