function(ft_set_profiling_options profiling_lib)
  set(PROFILING_OPTS)

  set(GCC_PROFILING_OPTS -pg)

  if(FT_ENABLE_PROFILING)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set(PROFILING_OPTS ${GCC_PROFILING_OPTS})
    else()
      message(FATAL_ERROR "Profiling is only supported for GCC")
    endif()
  endif()

  target_compile_options(${profiling_lib} INTERFACE ${PROFILING_OPTS})
  target_link_options(${profiling_lib} INTERFACE ${PROFILING_OPTS})
endfunction()
