function(ft_prevent_in_source_builds)
  file(REAL_PATH ${CMAKE_SOURCE_DIR} _src_dir)
  file(REAL_PATH ${CMAKE_BINARY_DIR} _bin_dir)
  if(${_src_dir} STREQUAL ${_bin_dir})
    message(FATAL_ERROR "In source builds are disallowed, please use a different build directory")
  endif()
endfunction()

ft_prevent_in_source_builds()
