# TODO: Rewrite function to accept name of file to be generated and name of generated file, along
# with the TARGET to link to. This should clean up the function a bit.

# ~~~
# ft_generate_source
#
# Generate a source with variables set in CMake.
#
# Arguments:
# SOURCE  name of source to be generated from
# TARGET  target that the source is generated for, directory of generated file
#         will be automatically added to target include directories
# ~~~
function(ft_generate_source)
  set(options)
  set(oneValueArgs SOURCE TARGET)
  set(multiValueArgs)
  cmake_parse_arguments(
    ""
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
  )

  # Insert 'generated' into file name and generate file.
  string(
    REGEX
    REPLACE "\.in$"
            ""
            source
            ${_SOURCE}
  )
  string(
    REPLACE "/"
            ";"
            path_list
            ${source}
  )
  list(LENGTH path_list num_path_elements)
  math(EXPR second_to_last "${num_path_elements} - 1")
  list(
    INSERT
    path_list
    ${second_to_last}
    "generated"
  )
  list(
    JOIN
    path_list
    "/"
    generated_path
  )
  configure_file(${_SOURCE} ${generated_path})

  # Add directory of generated file to target include directories.
  string(
    REPLACE "/"
            ";"
            generated_path_list
            ${generated_path}
  )
  list(POP_BACK generated_path_list)
  list(
    JOIN
    generated_path_list
    "/"
    generated_dir
  )
  if(_TARGET)
    target_include_directories(${_TARGET} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/${generated_dir})
  endif()
endfunction()
