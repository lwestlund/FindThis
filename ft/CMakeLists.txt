# Boost options.
find_package(Boost 1.71.0 REQUIRED COMPONENTS program_options regex)

add_library(compiler_warnings INTERFACE)
ft_set_compiler_warnings(compiler_warnings)

add_library(profiling_opts INTERFACE)
ft_set_profiling_options(profiling_opts)

if(FT_ENABLE_TESTING)
  add_subdirectory(test)
endif()

add_library(
  libft STATIC
  src/color.cpp
  src/file_types.cpp
  src/ft.cpp
  src/match.cpp
  src/parser.cpp
  src/search.cpp
)

set_target_properties(libft PROPERTIES OUTPUT_NAME ft)

ft_generate_source(SOURCE src/version.hpp.in TARGET libft)

target_include_directories(libft PUBLIC include)

target_link_libraries(libft PRIVATE ${Boost_LIBRARIES} compiler_warnings profiling_opts)

add_executable(ft src/main.cpp)

target_link_libraries(ft PRIVATE libft compiler_warnings profiling_opts)
