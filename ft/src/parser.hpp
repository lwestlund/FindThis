#ifndef FT_PARSER_HPP_
#define FT_PARSER_HPP_

#include "result.hpp"

#include <ft/ft.hpp>

#include <string>
#include <vector>

namespace ft
{
enum struct ParseError
{
  PrintHelp,
  PrintVersion,
  PrintTypelist,
  NoPattern,
  UnknownOption,
  MultipleSameOptionError,
  MissingOptionValueError,
};

struct ParseResults {
  Options options;
  std::string pattern;
};

Result<ParseResults, ParseError> parse(const std::vector<std::string>& args);

}  // namespace ft

#endif  // FT_PARSER_HPP_
