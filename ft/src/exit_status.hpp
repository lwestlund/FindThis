#ifndef FT_EXIT_STATUS_HPP_
#define FT_EXIT_STATUS_HPP_

namespace ft
{
enum ExitStatus : int
{
  Success = 0,
  Error
};

}  // namespace ft

#endif  // FT_EXIT_STATUS_HPP_
