#ifndef FT_MAP_HPP_
#define FT_MAP_HPP_

#include <algorithm>
#include <array>
#include <concepts>
#include <cstddef>
#include <optional>
#include <utility>

namespace ft
{
template<typename Key, typename Value>
struct MapPair {
  Key key;
  Value value;
};

template<typename Key, typename Value, std::size_t SIZE>
requires std::equality_comparable<Key>
class Map
{
public:
  constexpr Map(const std::array<MapPair<Key, Value>, SIZE>& map) : m_map{map}
  {
  }

  [[nodiscard]] constexpr std::optional<Value> at(const Key& key) const
  {
    const auto res =
        std::find_if(begin(m_map), end(m_map), [&key](const auto& p) { return p.key == key; });
    if (res != m_map.end()) {
      return {res->value};
    }
    return {};
  }

private:
  const std::array<MapPair<Key, Value>, SIZE> m_map;
};
}  // namespace ft

#endif  // FT_MAP_HPP_
