#include "assert.hpp"
#include "file_types.hpp"
#include "macros.hpp"
#include "parser.hpp"

#include <ft/ft.hpp>

#include <boost/xpressive/detail/core/icase.hpp>
#include <boost/xpressive/regex_constants.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/xpressive/xpressive_fwd.hpp>

#include <filesystem>
#include <fstream>
#include <ios>
#include <iostream>
#include <iterator>
#include <ostream>
#include <string>
#include <string_view>
#include <vector>

namespace ft
{
namespace
{
namespace xp = boost::xpressive;
namespace fs = std::filesystem;

xp::sregex getPattern(std::string_view patternString, const Options& options)
{
  xp::regex_constants::syntax_option_type regexOptions = xp::regex_constants::optimize;
  if (options.ignoreCaseSet && !options.caseSensitiveSet) {
    regexOptions = regexOptions | xp::regex_constants::icase;
  }
  const xp::sregex re = xp::sregex::compile(patternString, regexOptions);
  return re;
}

StringMatch searchString(const std::string& string, const xp::sregex& pattern)
{
  std::vector<StringMatch::MatchPosition> matchPositions;
  const auto stringBegin = string.cbegin();
  xp::sregex_iterator regexIterator(stringBegin, string.end(), pattern);
  xp::sregex_iterator regexEnd;
  for (; regexIterator != regexEnd; ++regexIterator) {
    const xp::smatch& match = *regexIterator;
    int matchStart = static_cast<int>(match[0].first - stringBegin);
    int matchEnd = static_cast<int>(match[0].second - stringBegin);
    matchPositions.emplace_back(matchStart, matchEnd);
  }
  return {string, matchPositions};
}

void searchLine(const std::string& line,
                const int lineNumber,
                const xp::sregex& pattern,
                std::vector<LineMatch>& lineMatches)
{
  const StringMatch stringMatch = searchString(line, pattern);
  if (!stringMatch.positions.empty()) {
    lineMatches.emplace_back(stringMatch, lineNumber);
  }
}

FileMatch searchFile(const fs::path& path, const xp::sregex& pattern)
{
  std::ifstream ifs(path);
  if (!ifs.is_open()) {
    return {"[error]: Unable to open file '" + path.string() + "'.", {}};
  }
  std::vector<LineMatch> lineMatches;
  int lineNumber = 1;
  for (std::string line; std::getline(ifs, line); ++lineNumber) {
    searchLine(line, lineNumber, pattern, lineMatches);
  }
  ifs.close();
  auto pathString = path.string();
  if (pathString.starts_with("./")) {
    auto pathWithoutLeadingDotSlash = pathString.substr(2);
    return {pathWithoutLeadingDotSlash, lineMatches};
  }
  return {pathString, lineMatches};
}

StringMatch searchFilename(const fs::path& path, const xp::sregex& pattern)
{
  StringMatch sm = searchString(path.filename().string(), pattern);
  auto parentPathString = path.parent_path().string();
  if (parentPathString.starts_with("./")) {
    parentPathString = parentPathString.substr(2);
  }
  if (!parentPathString.empty()) {
    const auto parentPathWithSeparatorLength = parentPathString.length() + 1;
    sm.string.reserve(sm.string.length() + parentPathWithSeparatorLength);
    sm.string.insert(0, parentPathString + fs::path::preferred_separator);
    for (auto& pos : sm.positions) {
      pos.start += parentPathWithSeparatorLength;
      pos.end += parentPathWithSeparatorLength;
    }
  }
  return sm;
}

SearchResults searchDirectory(const fs::path& directory,
                              const xp::sregex& pattern,
                              const Options& options)
{
  std::vector<FileMatch> fileMatches;
  std::vector<StringMatch> filenameMatches;
  for (const auto& dirEntry : fs::recursive_directory_iterator(directory)) {
    const auto& path = dirEntry.path();
    if (searchThisFileType(path, options.includeTypes, options.excludeTypes)) {
      if (options.searchFilenames) {
        // TODO(lovew): Add support to search for either name of directory or file.
        // This changes whether searchFilename is called depending on dirEntry.is_regular_file()
        // or dirEntry.is_directory().
        auto filenameMatch = searchFilename(path, pattern);
        if (!filenameMatch.positions.empty()) {
          filenameMatches.push_back(std::move(filenameMatch));
        }
      } else {
        if (!dirEntry.is_regular_file()) {
          continue;
        }
        auto fileMatch = searchFile(path, pattern);
        if (!fileMatch.lineMatches.empty()) {
          fileMatches.push_back(std::move(fileMatch));
        }
      }
    }
  }
  if (!fileMatches.empty()) {
    return {.fileMatches{fileMatches}};
  }
  if (!filenameMatches.empty()) {
    return {.filenameMatches{filenameMatches}};
  }
  return {};
}

}  // namespace

SearchResults search(std::string_view patternString, const Options& options)
{
  if (patternString.empty()) {
    return {.error{SearchError{.reason{SearchError::Reason::EmptyPattern}}}};
  }

  const auto unknownFileTypes = getUnknownFileTypes(options.includeTypes, options.excludeTypes);
  if (unknownFileTypes) {
    std::string unknowns;
    for (const auto& unknown : *unknownFileTypes) {
      unknowns += unknown + ' ';
    }
    return {.error{SearchError{.reason{SearchError::Reason::UnknownFileTypes}, .info{unknowns}}}};
  }

  const xp::sregex& pattern = getPattern(patternString, options);

  std::vector<FileMatch> fileMatches;
  std::vector<StringMatch> filenameMatches;
  for (const auto& path : options.searchPaths) {
    if (fs::is_regular_file(path)) {
      if (options.searchFilenames) {
        // Only directories are valid search paths when looking for filenames.
        StringMatch sm{};
        sm.string = "[error]: Path '" + path.string() + "' is not a directory.";
        const auto length = sm.string.length();
        sm.positions.emplace_back(length, length);
        filenameMatches.push_back(std::move(sm));
        continue;
      }
      auto fileMatch = searchFile(path, pattern);
      if (!fileMatch.lineMatches.empty()) {
        fileMatches.push_back(std::move(fileMatch));
      }
    } else if (fs::is_directory(path)) {
      auto newResults = searchDirectory(path, pattern, options);
      if (newResults.fileMatches) {
        auto& newFileMatches = *(newResults.fileMatches);
        fileMatches.reserve(fileMatches.size() + newFileMatches.size());
        std::move(newFileMatches.begin(), newFileMatches.end(), std::back_inserter(fileMatches));
      } else if (newResults.filenameMatches) {
        auto& newFilenameMatches = *(newResults.filenameMatches);
        filenameMatches.reserve(filenameMatches.size() + newFilenameMatches.size());
        std::move(newFilenameMatches.begin(),
                  newFilenameMatches.end(),
                  std::back_inserter(filenameMatches));
      }
    }
  }

  SearchResults results;
  if (!fileMatches.empty()) {
    results.fileMatches = std::move(fileMatches);
  } else if (!filenameMatches.empty()) {
    results.filenameMatches = std::move(filenameMatches);
  } else {
    // Found no matches.
  }
  return results;
}
}  // namespace ft
