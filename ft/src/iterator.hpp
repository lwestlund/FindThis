#ifndef FT_ITERATOR_HPP_
#define FT_ITERATOR_HPP_

#include "assert.hpp"

#include <iterator>

namespace ft
{
template<typename T>
class ConstIterator
{
public:
  using value_type = T;
  using reference = const T&;
  using difference_type = std::ptrdiff_t;
  using pointer = const T*;
  using iterator_concept = std::contiguous_iterator_tag;

  constexpr ConstIterator() = default;

  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr ConstIterator(const T* ptr) : m_ptr{ptr}
  {
  }

  constexpr auto operator<=>(const ConstIterator& other) const = default;

  constexpr ConstIterator& operator++()
  {
    ++m_ptr;
    return *this;
  }

  constexpr ConstIterator operator++(int)
  {
    ConstIterator tmp = *this;
    ++(*this);
    return tmp;
  }

  constexpr ConstIterator& operator+=(const difference_type iterDifference)
  {
    m_ptr += iterDifference;
    return *this;
  }

  constexpr ConstIterator operator+(const difference_type iterDifference) const
  {
    return {m_ptr + iterDifference};
  }

  friend constexpr ConstIterator operator+(const difference_type iterDifference,
                                           const ConstIterator& iter)
  {
    return {iter.m_ptr + iterDifference};
  }

  constexpr ConstIterator& operator--()
  {
    --m_ptr;
    return *this;
  }

  constexpr ConstIterator operator--(int)
  {
    const ConstIterator tmp = *this;
    --m_ptr;
    return tmp;
  }

  constexpr ConstIterator& operator-=(const difference_type iterDifference)
  {
    m_ptr -= iterDifference;
    return *this;
  }

  constexpr ConstIterator operator-(const difference_type iterDifference) const
  {
    return {m_ptr - iterDifference};
  }

  constexpr difference_type operator-(const ConstIterator& other) const
  {
    return m_ptr - other.m_ptr;
  }

  [[nodiscard]] constexpr reference operator[](const difference_type i) const
  {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return m_ptr[i];
  }

  constexpr reference operator*() const
  {
    FT_PRECONDITION(m_ptr != nullptr);
    return *m_ptr;
  }

  constexpr pointer operator->() const
  {
    return m_ptr;
  }

private:
  const T* m_ptr{};
};

template<typename T>
class Iterator
{
public:
  using value_type = T;
  using reference = T&;
  using difference_type = std::ptrdiff_t;
  using pointer = T*;
  using iterator_concept = std::contiguous_iterator_tag;

  constexpr Iterator() = default;

  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Iterator(T* const ptr) : m_ptr{ptr}
  {
  }

  constexpr auto operator<=>(const Iterator& other) const = default;

  constexpr Iterator& operator++()
  {
    ++m_ptr;
    return *this;
  }

  constexpr Iterator operator++(int)
  {
    Iterator tmp = *this;
    ++(*this);
    return tmp;
  }

  constexpr Iterator& operator+=(const difference_type iterDifference)
  {
    m_ptr += iterDifference;
    return *this;
  }

  constexpr Iterator operator+(const difference_type iterDifference) const
  {
    return {m_ptr + iterDifference};
  }

  friend constexpr Iterator operator+(const difference_type iterDifference, const Iterator& iter)
  {
    return {iter.m_ptr + iterDifference};
  }

  constexpr Iterator& operator--()
  {
    --m_ptr;
    return *this;
  }

  constexpr Iterator operator--(int)
  {
    Iterator tmp = *this;
    --(*this);
    return tmp;
  }

  constexpr Iterator& operator-=(const difference_type iterDifference)
  {
    m_ptr -= iterDifference;
    return *this;
  }

  constexpr Iterator operator-(const difference_type iterDifference) const
  {
    return {m_ptr - iterDifference};
  }

  constexpr difference_type operator-(const Iterator& other) const
  {
    return m_ptr - other.m_ptr;
  }

  [[nodiscard]] constexpr reference operator[](const difference_type i) const
  {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return m_ptr[i];
  }

  constexpr reference operator*() const
  {
    FT_PRECONDITION(m_ptr != nullptr);
    return *m_ptr;
  }

  constexpr pointer operator->() const
  {
    return m_ptr;
  }

private:
  T* m_ptr{};
};

template<typename T>
concept ContiguousIterator =
    std::contiguous_iterator<ConstIterator<T>> && std::contiguous_iterator<Iterator<T>>;
}  // namespace ft

#endif  // FT_ITERATOR_HPP_
