#include "exit_status.hpp"
#include "parser.hpp"

#include <ft/ft.hpp>

#include <iostream>
#include <string>
#include <vector>

int main(const int argc, const char* argv[])
{
  const std::vector<std::string> args(argv + 1, argv + argc);
  const auto parseResults = ft::parse(args);
  if (parseResults) {
    const auto searchResults = ft::search(parseResults->pattern, parseResults->options);
    ft::printSearchResults(searchResults);
    return ft::ExitStatus::Success;
  }

  return static_cast<int>(parseResults.error());
}
