#ifndef FT_VECTOR_HPP_
#define FT_VECTOR_HPP_

#include "assert.hpp"
#include "iterator.hpp"

#include <algorithm>
#include <cstddef>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <type_traits>

namespace ft
{
template<typename T, std::size_t SIZE>
requires ContiguousIterator<T>
class Vector
{
public:
  using const_iterator = ConstIterator<T>;
  using iterator = Iterator<T>;

  constexpr Vector() : m_size{0}, m_capacity{SIZE}, m_array{}
  {
  }

  constexpr Vector(const Vector& other) : m_size{other.m_size}, m_capacity{SIZE}
  {
    std::copy(other.cbegin(), other.cend(), &m_array[0]);
  }

  template<std::size_t OTHER_SIZE>
  constexpr Vector(const Vector<T, OTHER_SIZE>& other)
      : m_size{other.size()},
        m_capacity{SIZE},
        m_array{}
  {
    FT_STATIC_ASSERT(OTHER_SIZE <= SIZE);
    std::copy(other.cbegin(), other.cend(), &m_array[0]);
  }

  constexpr Vector(Vector&& other) noexcept : Vector()
  {
    swap(*this, other);
  }

  constexpr Vector(std::initializer_list<T> init) : m_size{init.size()}, m_capacity{SIZE}, m_array{}
  {
    FT_PRECONDITION(init.size() <= SIZE);
    std::size_t i = 0;
    for (T t : init) {
      m_array[i] = t;
      ++i;
    }
  }

  constexpr ~Vector() requires(!std::is_trivially_destructible_v<T>)
  {
    for (T& t : m_array) {
      t.~T();
    }
  }

  constexpr ~Vector() = default;

  [[nodiscard]] constexpr Vector& operator=(Vector other)
  {
    swap(*this, other);
    return *this;
  }

  constexpr Vector& operator=(std::initializer_list<T> init)
  {
    FT_PRECONDITION(init.size() <= SIZE);
    std::ranges::copy(init, &m_array[0]);
    m_size = init.size();
    return *this;
  }

  constexpr T& operator[](const std::size_t i)
  {
    FT_PRECONDITION(i < m_size);
    return m_array[i];
  }

  constexpr const T& operator[](const std::size_t i) const
  {
    FT_PRECONDITION(i < m_size);
    return m_array[i];
  }

  constexpr T& front()
  {
    FT_PRECONDITION(m_size > 0);
    return m_array[0];
  }

  constexpr const T& front() const
  {
    FT_PRECONDITION(m_size > 0);
    return m_array[0];
  }

  constexpr T& back()
  {
    FT_PRECONDITION(m_size > 0);
    return m_array[m_size - 1];
  }

  constexpr const T& back() const
  {
    FT_PRECONDITION(m_size > 0);
    return m_array[m_size - 1];
  }

  constexpr iterator begin()
  {
    return &m_array[0];
  }

  constexpr const_iterator begin() const
  {
    return &m_array[0];
  }

  constexpr const_iterator cbegin() const
  {
    return &m_array[0];
  }

  constexpr iterator end()
  {
    return &m_array[m_size];
  }

  constexpr const_iterator end() const
  {
    return &m_array[m_size];
  }

  constexpr const_iterator cend() const
  {
    return &m_array[m_size];
  }

  constexpr std::size_t size() const
  {
    return m_size;
  }

  constexpr std::size_t capacity() const
  {
    return m_capacity;
  }

  constexpr void clear() requires(std::is_trivially_destructible_v<T>)
  {
    m_size = 0;
  }

  constexpr void clear() requires(!std::is_trivially_destructible_v<T>)
  {
    for (T& t : m_array) {
      t.~T();
    }
    m_size = 0;
  }

  constexpr void pushBack(const T& element)
  {
    FT_PRECONDITION(m_size < m_capacity);
    m_array[m_size] = element;
    ++m_size;
  }

  template<class... Args>
  constexpr T& emplaceBack(Args&&... args)
  {
    FT_PRECONDITION(m_size < m_capacity);
    new (&m_array[m_size]) T(std::move(args)...);
    ++m_size;
    return back();
  }

  friend constexpr void swap(Vector& lhs, Vector& rhs)
  {
    using std::swap;

    swap(lhs.m_size, rhs.m_size);
    swap(lhs.m_capacity, rhs.m_capacity);
    swap(lhs.m_array, rhs.m_array);
  }

private:
  std::size_t m_size;
  std::size_t m_capacity;
  T m_array[SIZE];
};
}  // namespace ft

#endif  // FT_VECTOR_HPP_
