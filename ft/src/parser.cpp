#include "parser.hpp"

#include "file_types.hpp"
#include "result.hpp"
#include "version.hpp"

#include <boost/program_options.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>

#include <iostream>
#include <iterator>
#include <ostream>
#include <string>
#include <string_view>
#include <unordered_set>
#include <vector>

namespace ft
{
namespace po = boost::program_options;

namespace
{
constexpr std::string_view AUTHOR{"Love Westlund"};
constexpr std::string_view URL{"https://github.com/lovewestlundgotby/FindThis"};
constexpr std::string_view DESCRIPTION{
    "FindThis (ft) recursively searches for files or "
    "in files for a regex pattern."};

void printUsage(std::ostream& out, const po::options_description& optionsDescription)
{
  const std::string_view usage{"Usage:\n  ft [OPTIONS] PATTERN [PATH ...]\n"};
  out << usage << optionsDescription << std::endl;
}

void printHelp(std::ostream& out, const po::options_description& optionsDescription)
{
  out << "ft " << FT_VERSION << "\n" << AUTHOR << "\n" << URL << "\n\n" << DESCRIPTION << "\n\n";
  printUsage(out, optionsDescription);
}

void logError(const std::string_view msg, const po::options_description& options)
{
  std::cerr << "error: " << msg << "\n\n";
  printUsage(std::cerr, options);
}
}  // namespace

Result<ParseResults, ParseError> parse(const std::vector<std::string>& args)
{
  ParseResults results;
  std::vector<std::string> includeTypes;
  std::vector<std::string> excludeTypes;
  // clang-format off
  po::options_description arguments("Positional arguments");
  arguments.add_options()
    ("pattern",
     po::value<std::string>(&results.pattern)
       ->required()
       ->value_name("PATTERN"),
     "Search for pattern.")
    ("path",
     po::value<std::vector<std::filesystem::path>>(&results.options.searchPaths)
       ->multitoken()
       ->composing()
       ->value_name("PATH"),
     "Search path.");

  po::options_description options("Options");
  options.add_options()
    ("filename,f",
     po::bool_switch(&results.options.searchFilenames),
     "Search for filenames matching PATTERN.")
    ("ignore-case,i", po::bool_switch(&results.options.ignoreCaseSet), "Search case insensitively.")
    ("case-sensitive,s",
     po::bool_switch(&results.options.caseSensitiveSet),
     "Search case sensitively, overrides --ignore-case.")
    ("type,t", po::value<decltype(includeTypes)>(&includeTypes)->multitoken()->composing(),
     "Search for files with type, overrides --not-type.")
    ("not-type,T", po::value<decltype(excludeTypes)>(&excludeTypes)->multitoken()->composing(),
     "Search for files not of type, overridden by --type.")
    ("type-list", "Print list of supported file types.");

  po::options_description generic("Generic options");
  generic.add_options()
    ("help,h", "Prints help message.")
    ("version,V", "Prints version information.");
  // clang-format on

  po::options_description cmdlineOptions;
  cmdlineOptions.add(arguments).add(options).add(generic);

  po::positional_options_description p;
  p.add("pattern", 1);
  p.add("path", -1);

  po::command_line_parser parser(args);
  const auto style =
      // NOLINTNEXTLINE(hicpp-signed-bitwise)
      (po::command_line_style::allow_short | po::command_line_style::short_allow_adjacent |
       po::command_line_style::short_allow_next | po::command_line_style::allow_dash_for_short |
       po::command_line_style::allow_long | po::command_line_style::long_allow_adjacent |
       po::command_line_style::long_allow_next | po::command_line_style::allow_sticky);
  parser.options(cmdlineOptions).positional(p).style(style);

  try {
    const po::parsed_options parsedOptions = parser.run();
    po::variables_map vm;
    po::store(parsedOptions, vm);

    if (vm.contains("help")) {
      printHelp(std::cout, cmdlineOptions);
      return ParseError::PrintHelp;
    }
    if (vm.contains("version")) {
      std::cout << "ft " << FT_VERSION << "\n" << COPYRIGHT << '\n';
      return ParseError::PrintVersion;
    }
    if (vm.contains("type-list")) {
      std::cout << getFileTypeListString();
      return ParseError::PrintTypelist;
    }

    po::notify(vm);
  } catch (po::required_option& e) {
    logError(e.what(), cmdlineOptions);
    return ParseError::NoPattern;
  } catch (po::invalid_command_line_syntax& e) {
    std::cerr << "foo\n";
    logError(e.what(), cmdlineOptions);
    return ParseError::MissingOptionValueError;
  } catch (po::unknown_option& e) {
    logError(e.what(), cmdlineOptions);
    return ParseError::UnknownOption;
  } catch (po::multiple_occurrences& e) {
    std::cerr << "bar\n";
    logError(e.what(), cmdlineOptions);
    return ParseError::MultipleSameOptionError;
  }

  // Move parsed include/exclude filetypes into the options, since we need to parse them into a
  // vector first.
  std::move(includeTypes.begin(),
            includeTypes.end(),
            std::inserter(results.options.includeTypes, results.options.includeTypes.end()));
  std::move(excludeTypes.begin(),
            excludeTypes.end(),
            std::inserter(results.options.excludeTypes, results.options.excludeTypes.end()));

  return results;
}

}  // namespace ft
