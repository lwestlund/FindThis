#include "color.hpp"

#include <cstdio>
#include <iostream>
#include <ostream>
#include <unistd.h>

namespace ft
{
namespace
{
bool printingToTerminal(std::ostream& os)
{
  FILE* stream = nullptr;
  if (&os == &std::cout) {
    stream = stdout;
  } else if (&os == &std::cerr || &os == &std::clog) {
    stream = stderr;
  } else {
    // If it's neither of cout, cerr, or clog we assume that we are printing to a terminal.
    return true;
  }
  return isatty(fileno(stream));
}

}  // namespace

std::ostream& operator<<(std::ostream& out, const Color color)
{
  static bool firstUse = true;
  static bool isPrintingToTerminal;
  if (firstUse) {
    firstUse = false;
    isPrintingToTerminal = printingToTerminal(out);
  }

  if (isPrintingToTerminal) {
    return out << "\033[" << static_cast<int>(color) << "m";
  } else {
    return out;
  }
}
}  // namespace ft
