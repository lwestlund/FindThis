#include "color.hpp"

#include <ft/ft.hpp>

#include <ostream>
#include <string_view>
#include <vector>

namespace ft
{
std::ostream& operator<<(std::ostream& out, const StringMatch& stringMatch)
{
  const std::string_view str = stringMatch.string;
  const auto& mps = stringMatch.positions;

  const auto& firstMp = mps.front();
  const auto& lastMp = mps.back();
  out << str.substr(0, firstMp.start);
  out << Color::FG_BRIGHT_RED << Color::BOLD << Color::UNDERLINE
      << str.substr(firstMp.start, firstMp.end - firstMp.start) << Color::RESET;

  if (mps.size() > 1) {
    for (std::size_t i = 1; i < mps.size(); ++i) {
      const auto& mp = mps[i];
      const auto& previousMP = mps[i - 1];
      out << str.substr(previousMP.end, mp.start - previousMP.end);
      out << Color::FG_BRIGHT_RED << Color::BOLD << Color::UNDERLINE
          << str.substr(mp.start, mp.end - mp.start) << Color::RESET;
    }
  }
  out << str.substr(lastMp.end) << '\n';

  return out;
}

std::ostream& operator<<(std::ostream& out, const LineMatch& lineMatch)
{
  out << Color::FG_BRIGHT_YELLOW << lineMatch.lineNumber << Color::RESET << ":"
      << lineMatch.stringMatch;
  return out;
}

std::ostream& operator<<(std::ostream& out, const FileMatch& fileMatch)
{
  if (!fileMatch.filename.empty()) {
    out << Color::FG_BRIGHT_MAGENTA << fileMatch.filename << Color::RESET << '\n';
  }
  for (const LineMatch& lineMatch : fileMatch.lineMatches) {
    out << lineMatch;
  }
  return out;
}
}  // namespace ft
