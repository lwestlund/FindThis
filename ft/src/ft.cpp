#include <ft/ft.hpp>

#include <string_view>

namespace ft
{
namespace
{
std::string_view errorReasonToString(const SearchError::Reason reason)
{
  switch (reason) {
    case SearchError::Reason::EmptyPattern:
      return "Empty pattern";
    case SearchError::Reason::UnknownFileTypes:
      return "Unknown file types";
  }
  return "Unknown error reason";
}
}  // namespace

std::ostream& operator<<(std::ostream& out, const SearchResults& searchResults)
{
  if (searchResults.fileMatches) {
    const auto& fileMatches = *(searchResults.fileMatches);
    for (const auto& fileMatch : fileMatches) {
      if (!fileMatch.lineMatches.empty()) {
        out << fileMatch;
      }
    }
  }
  if (searchResults.filenameMatches) {
    const auto& filenameMatches = *(searchResults.filenameMatches);
    for (const auto& filenameMatch : filenameMatches) {
      if (!filenameMatch.positions.empty()) {
        out << filenameMatch;
      }
    }
  }
  out << '\n';
  return out;
}

void printSearchResults(const SearchResults& results, std::ostream& out)
{
  if (results.error) {
    out << "[error]: " << errorReasonToString(results.error->reason) << '\n';
  } else {
    out << results;
  }
}
}  // namespace ft
