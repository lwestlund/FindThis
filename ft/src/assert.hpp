#ifndef FT_ASSERT_HPP_
#define FT_ASSERT_HPP_

#include <cassert>

namespace ft
{
#define FT_CONSTEVAL_STATIC_ASSERT(condition, msg) \
  do {                                             \
    if (!(condition)) {                            \
      throw msg;                                   \
    }                                              \
  } while (false)

#define FT_STATIC_ASSERT(condition) static_assert(condition)

#if defined(NDEBUG)
#define FT_PRECONDITION(condition)
#else
#define FT_PRECONDITION(condition) assert(condition)
#endif

#if defined(NDEBUG)
#define FT_POSTCONDITION(condition)
#else
#define FT_POSTCONDITION(condition) assert(condition)
#endif
}  // namespace ft

#endif  // FT_ASSERT_HPP_
