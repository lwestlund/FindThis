#ifndef FT_RESULT_HPP_
#define FT_RESULT_HPP_

#include "assert.hpp"

#include <utility>
#include <variant>

namespace ft
{
template<typename R, typename E>
class Result
{
public:
  Result() = delete;

  // Allow implicit conversions, otherwise we cannot do `return r`, where `r` is an `R`, in a
  // function returning a Result<R, E>.
  //
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Result(const R& r) : m_value{r}, m_hasResult{true}
  {
  }

  // Allow implicit conversions, otherwise we cannot do `return r`, where `r` is an `R`, in a
  // function returning a Result<R, E>.
  //
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Result(R&& r) : m_value{R{}}, m_hasResult{true}
  {
    using std::swap;
    swap(std::get<R>(m_value), r);
  }

  // Allow implicit conversions, otherwise we cannot do `return e`, where `e` is an `E`, in a
  // function returning a Result<R, E>.
  //
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Result(const E& e) : m_value{e}, m_hasResult{false}
  {
  }

  // Allow implicit conversions, otherwise we cannot do `return e`, where `e` is an `E`, in a
  // function returning a Result<R, E>.
  //
  // NOLINTNEXTLINE(google-explicit-constructor)
  constexpr Result(E&& e) : m_value{E{}}, m_hasResult{false}
  {
    using std::swap;
    swap(std::get<E>(m_value), e);
  }

  constexpr Result(const Result& other) : m_value(other.m_value), m_hasResult(other.m_hasResult)
  {
  }

  constexpr Result(Result&& other) noexcept : m_hasResult{false}
  {
    swap(*this, other);
  }

  constexpr ~Result() = default;

  [[nodiscard]] constexpr Result& operator=(Result other)
  {
    using std::swap;
    swap(m_value, other.m_value);
    swap(m_hasResult, other.m_hasResult);
  }

  [[nodiscard]] constexpr Result& operator=(Result&& other) noexcept
  {
    using std::swap;
    swap(m_value, other.m_value);
    swap(m_hasResult, other.m_hasResult);
  }

  [[nodiscard]] constexpr Result& operator=(R r)
  {
    m_value = std::move(r);
    m_hasResult = true;
    return *this;
  }

  [[nodiscard]] constexpr Result& operator=(E e)
  {
    m_value = std::move(e);
    m_hasResult = false;
    return *this;
  }

  [[nodiscard]] constexpr bool hasResult() const
  {
    return m_hasResult;
  }

  [[nodiscard]] constexpr explicit operator bool() const
  {
    return hasResult();
  }

  [[nodiscard]] constexpr R& operator*()
  {
    FT_PRECONDITION(m_hasResult);
    FT_PRECONDITION(std::holds_alternative<R>(m_value));
    return std::get<R>(m_value);
  }

  [[nodiscard]] constexpr const R& operator*() const
  {
    FT_PRECONDITION(m_hasResult);
    FT_PRECONDITION(std::holds_alternative<R>(m_value));
    return std::get<R>(m_value);
  }

  [[nodiscard]] constexpr R* operator->()
  {
    FT_PRECONDITION(m_hasResult);
    FT_PRECONDITION(std::holds_alternative<R>(m_value));
    return &std::get<R>(m_value);
  }

  [[nodiscard]] constexpr const R* operator->() const
  {
    FT_PRECONDITION(m_hasResult);
    FT_PRECONDITION(std::holds_alternative<R>(m_value));
    return &std::get<R>(m_value);
  }

  [[nodiscard]] constexpr const E& error() const
  {
    FT_PRECONDITION(!m_hasResult);
    FT_PRECONDITION(std::holds_alternative<E>(m_value));
    return std::get<E>(m_value);
  }

  friend constexpr void swap(Result& lhs, Result& rhs)
  {
    lhs.m_value.swap(rhs.m_value);
    using std::swap;
    swap(lhs.m_hasResult, rhs.m_hasResult);
  }

private:
  std::variant<R, E> m_value;
  bool m_hasResult;
};
}  // namespace ft

#endif  // FT_RESULT_HPP_
