#ifndef FT_FILE_TYPES_HPP_
#define FT_FILE_TYPES_HPP_

#include <filesystem>
#include <optional>
#include <string>
#include <string_view>
#include <unordered_set>

namespace ft
{
namespace fs = std::filesystem;

std::optional<std::unordered_set<std::string>> getUnknownFileTypes(
    const std::unordered_set<std::string>& includeTypes,
    const std::unordered_set<std::string>& excludeTypes);

bool searchThisFileType(const fs::path& path,
                        const std::unordered_set<std::string>& includeTypes,
                        const std::unordered_set<std::string>& excludeTypes);

std::string getFileTypeListString();
}  // namespace ft

#endif  // FT_FILE_TYPES_HPP_
