#include "file_types.hpp"

#include "color.hpp"
#include "macros.hpp"
#include "map.hpp"
#include "vector.hpp"

#include <algorithm>
#include <cstring>
#include <filesystem>
#include <iostream>
#include <sstream>
#include <string_view>
#include <unordered_set>
#include <utility>
#include <vector>

namespace ft
{
namespace fs = std::filesystem;

namespace
{
constexpr int MAX_LINE_LENGTH = 80;

// We need to set a value, but this can be changed if needed.
constexpr int MAX_NUM_FILETYPES = 12;
using Key = std::string_view;
using Value = Vector<std::string_view, MAX_NUM_FILETYPES>;
using Pair = MapPair<Key, Value>;
constexpr Pair bash{
    "bash",
    {".bash", ".bash_login", ".bash_profile", ".bashrc", ".login", ".logout", ".profile"}};
constexpr Pair c{"c", {".c", ".h", ".h.in"}};
constexpr Pair cmake{"cmake", {".cmake", "CMakeLists.txt"}};
constexpr Pair config{"config", {".cfg", ".conf", ".config"}};
constexpr Pair cpp{"cpp",
                   {".cpp", ".hpp", ".hpp.in", ".cxx", ".hxx", ".hxx.in", ".cc", ".hh", ".hh.in"}};
constexpr Pair csv{"csv", {".csv", ".tsv"}};
constexpr Pair elisp{"elisp", {".el"}};
constexpr Pair fish{"fish", {".fish"}};
constexpr Pair groovy{"groovy", {".groovy"}};
constexpr Pair jpeg{"jpeg", {".jpeg", ".jpg"}};
constexpr Pair json{"json", {".json"}};
constexpr Pair lisp{"lisp", {".el", ".lisp", ".lsp"}};
constexpr Pair markdown{"markdown", {".markdown", ".md", ".mkdn"}};
constexpr Pair org{"org", {".org"}};
constexpr Pair pdf{"pdf", {".pdf"}};
constexpr Pair png{"png", {".png"}};
constexpr Pair protobuf{"protobuf", {".pb", ".proto"}};
constexpr Pair python{"python", {".py"}};
constexpr Pair rst{"rst", {".rst"}};
constexpr Pair sh{
    "sh",
    {".bash", ".bash_login", ".bash_profile", ".bashrc", ".login", ".logout", ".profile", ".sh"}};
constexpr Pair tar{"tar", {".tar", ".tar.bz", ".tar.gz", ".tar.xz"}};
constexpr Pair txt{"txt", {".txt"}};
constexpr Pair vim{"vim", {".vim", ".vimrc"}};
constexpr Pair yaml{"yaml", {".yaml", ".yml"}};
constexpr Pair zsh{"zsh", {".zlogin", ".zlogout", ".zprofile", ".zsh", ".zshenv", ".zshrc"}};
constexpr std::array FILE_TYPE_PAIRS{bash,     c,      cmake, config, cpp,      csv, elisp, fish,
                                     groovy,   jpeg,   json,  lisp,   markdown, org, pdf,   png,
                                     protobuf, python, rst,   tar,    txt,      vim, yaml,  zsh};

constexpr Map FILE_TYPE_EXTENSIONS(FILE_TYPE_PAIRS);

bool extensionMatch(const fs::path& pathFilename, const std::unordered_set<std::string>& extensions)
{
  const char* const pathFilenameStr = pathFilename.c_str();
  const char* const extensionFromFirstDot = strchr(pathFilenameStr, '.');
  // const auto& extensionCurrentFile = path.extension();
  for (const auto& extensionToMatch : extensions) {
    // Check special cases.
    if (extensionToMatch == "cmake") {
      if (pathFilename == "CMakeLists.txt") {
        return true;
      }
    }

    const auto findResult = FILE_TYPE_EXTENSIONS.at(extensionToMatch);
    if (findResult) {
      for (const std::string_view fileExtension : *findResult) {
        if (extensionFromFirstDot && fileExtension.compare(extensionFromFirstDot) == 0) {
          return true;
        }
      }
    } else {
      std::abort();
    }
  }
  return false;
}

}  // namespace

std::optional<std::unordered_set<std::string>> getUnknownFileTypes(
    const std::unordered_set<std::string>& includeTypes,
    const std::unordered_set<std::string>& excludeTypes)
{
  std::unordered_set<std::string> unknownFileTypes;

  for (const auto& fileType : includeTypes) {
    if (!FILE_TYPE_EXTENSIONS.at(fileType)) {
      unknownFileTypes.insert(fileType);
    }
  }
  for (const auto& fileType : excludeTypes) {
    if (!FILE_TYPE_EXTENSIONS.at(fileType)) {
      unknownFileTypes.insert(fileType);
    }
  }

  if (!unknownFileTypes.empty()) {
    return unknownFileTypes;
  }
  return {};
}

bool searchThisFileType(const fs::path& path,
                        const std::unordered_set<std::string>& includeTypes,
                        const std::unordered_set<std::string>& excludeTypes)
{
  if (includeTypes.empty() && excludeTypes.empty()) {
    return true;
  }

  const bool explicitelyInclude{extensionMatch(path.filename(), includeTypes)};
  const bool explicitelyExclude{extensionMatch(path.filename(), excludeTypes)};

  return (explicitelyInclude || (!explicitelyExclude && includeTypes.empty()));
}

std::string getFileTypeListString()
{
  std::string typeListString;
  for (const Pair& pair : FILE_TYPE_PAIRS) {
    std::ostringstream oss;
    oss << Color::FG_BRIGHT_MAGENTA << pair.key << Color::RESET << ": ";
    const std::size_t indentLength = pair.key.length() + 2;
    std::size_t lineLength = indentLength;
    const auto numTypes = pair.value.size();
    for (std::size_t i = 0; i < numTypes - 1; ++i) {
      const std::string_view extension = pair.value[i];
      const auto numCharsToAdd = extension.length() + 3;  // Add three for '*' and ', '.
      if (lineLength + numCharsToAdd > MAX_LINE_LENGTH) {
        oss << "\n";
        for (std::size_t j = 0; j < indentLength; ++j) {
          oss << " ";
        }
        lineLength = indentLength;
      }
      oss << '*' << extension << ", ";
      lineLength += numCharsToAdd;
    }
    // Print last type without trailing comma.
    const std::string_view extension = pair.value.back();
    if (lineLength + extension.length() + 1 > MAX_LINE_LENGTH) {
      oss << "\n";
      for (std::size_t j = 0; j < indentLength; ++j) {
        oss << " ";
      }
    }
    oss << '*' << extension << '\n';
    typeListString += oss.str();
  }
  return typeListString;
}
}  // namespace ft
