#ifndef FT_HPP_
#define FT_HPP_

#include <filesystem>
#include <iostream>
#include <optional>
#include <ostream>
#include <string>
#include <string_view>
#include <unordered_set>
#include <vector>

namespace ft
{
struct StringMatch {
  struct MatchPosition {
    std::size_t start;
    std::size_t end;
  };

  std::string string;
  std::vector<MatchPosition> positions;

  friend std::ostream& operator<<(std::ostream& out, const StringMatch& stringMatch);
};

struct LineMatch {
  StringMatch stringMatch;
  int lineNumber;

  friend std::ostream& operator<<(std::ostream& out, const LineMatch& lineMatch);
};

struct FileMatch {
  std::string filename;
  std::vector<LineMatch> lineMatches;

  friend std::ostream& operator<<(std::ostream& out, const FileMatch& fileMatch);
};

struct Options {
  std::vector<std::filesystem::path> searchPaths{"."};
  std::unordered_set<std::string> includeTypes{};
  std::unordered_set<std::string> excludeTypes{};
  bool searchFilenames{false};
  bool ignoreCaseSet{false};
  bool caseSensitiveSet{false};
};

struct SearchError {
  enum struct Reason
  {
    EmptyPattern,
    UnknownFileTypes,
  };

  Reason reason{};
  std::optional<std::string> info{};
};

struct SearchResults {
  std::optional<std::vector<FileMatch>> fileMatches{};
  std::optional<std::vector<StringMatch>> filenameMatches{};
  std::optional<SearchError> error{};

  friend std::ostream& operator<<(std::ostream& out, const SearchResults& searchResults);
};

[[nodiscard]] SearchResults search(std::string_view pattern, const Options& options);

void printSearchResults(const SearchResults& results, std::ostream& out = std::cout);
}  // namespace ft

#endif  // FT_HPP_
