#include "vector.hpp"

#include <doctest/doctest.h>

#include <algorithm>
#include <iostream>

namespace helpers
{
template<typename T, std::size_t SIZE>
constexpr auto getVector()
{
  ft::Vector<T, SIZE> vec;
  for (std::size_t i = 0; i < vec.capacity(); ++i) {
    vec.pushBack(static_cast<T>(i));
  }
  return vec;
}
}  // namespace helpers

// TODO(lovew): Type parametrize to have a non trivial template type, preferable with a non-trivial
// assignment operator.
TEST_CASE("Construct Vector")
{
  SUBCASE("constexpr Vector()")
  {
    // GIVEN
    constexpr ft::Vector<int, 5> vec;

    // THEN
    CHECK_EQ(vec.size(), 0);
    CHECK_EQ(vec.capacity(), 5);
  }

  SUBCASE("constexpr Vector(std::initializer_list<T>)")
  {
    // GIVEN
    constexpr ft::Vector<int, 5> vec{10, 20, 30};

    // THEN
    CHECK_EQ(vec.size(), 3);
    CHECK_EQ(vec.capacity(), 5);
    CHECK_EQ(vec[0], 10);
    CHECK_EQ(vec[1], 20);
    CHECK_EQ(vec[2], 30);
  }

  SUBCASE("constexpr Vector(const Vector&)")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec1{10, 20, 30};

    // WHEN
    constexpr ft::Vector<int, 3> vec2 = vec1;

    // THEN
    CHECK_EQ(vec2.size(), 3);
    CHECK_EQ(vec2[0], 10);
    CHECK_EQ(vec2[1], 20);
    CHECK_EQ(vec2[2], 30);
  }

  SUBCASE("Vector(const Vector<T, OTHER_SIZE>&)")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec1{10, 20, 30};

    // WHEN
    constexpr ft::Vector<int, 4> vec2 = vec1;

    // THEN
    CHECK_EQ(vec2.size(), 3);
    CHECK_EQ(vec2[0], 10);
    CHECK_EQ(vec2[1], 20);
    CHECK_EQ(vec2[2], 30);
  }

  SUBCASE("operator=(std::initializer_list<T>)")
  {
    // GIVEN
    ft::Vector<int, 5> vec;

    // WHEN
    vec = {10, 20};

    // THEN
    CHECK_EQ(vec.size(), 2);
    CHECK_EQ(vec[0], 10);
    CHECK_EQ(vec[1], 20);
  }

  SUBCASE("constexpr Vector(Vector&&)")
  {
    // WHEN
    constexpr auto vec{helpers::getVector<int, 2>()};

    // THEN
    CHECK_EQ(vec.size(), 2);
    CHECK_EQ(vec.capacity(), 2);
    CHECK_EQ(vec[0], 0);
    CHECK_EQ(vec[1], 1);
  }
}

TEST_CASE("Element access")
{
  SUBCASE("Subscript operator")
  {
    SUBCASE("operator[](const int)")
    {
      // GIVEN
      ft::Vector<int, 5> vec{10, 20, 30, 40, 50};
      CHECK_EQ(vec[4], 50);

      // WHEN
      vec[4] = 55;

      // THEN
      CHECK_EQ(vec[4], 55);
    }

    SUBCASE("operator[](const int) const")
    {
      // GIVEN
      constexpr ft::Vector<int, 5> vec{10, 20};

      // THEN
      CHECK_EQ(vec[0], 10);
      CHECK_EQ(vec[1], 20);
    }
  }

  SUBCASE("front()")
  {
    // GIVEN
    constexpr ft::Vector<int, 2> vec{10, 20};

    // WHEN
    auto& front = vec.front();

    // THEN
    CHECK_EQ(front, 10);
  }

  SUBCASE("front() const")
  {
    // GIVEN
    constexpr ft::Vector<int, 2> vec{10, 20};

    // WHEN
    const auto& front = vec.front();

    // THEN
    CHECK_EQ(front, 10);
  }

  SUBCASE("back()")
  {
    // GIVEN
    constexpr ft::Vector<int, 2> vec{10, 20};

    // WHEN
    auto& back = vec.back();

    // THEN
    CHECK_EQ(back, 20);
  }

  SUBCASE("back() const")
  {
    // GIVEN
    constexpr ft::Vector<int, 2> vec{10, 20};

    // WHEN
    const auto& back = vec.back();

    // THEN
    CHECK_EQ(back, 20);
  }
}

TEST_CASE("Iterators")
{
  SUBCASE("begin()/end()")
  {
    // GIVEN
    ft::Vector<int, 5> vec{0, 0, 0};

    int i = 0;
    for (auto& e : vec) {
      // WHEN
      e = i;
      ++i;
    }

    // THEN
    CHECK_EQ(vec.size(), i);
    CHECK_EQ(vec.size(), 3);
    CHECK_EQ(vec[0], 0);
    CHECK_EQ(vec[1], 1);
    CHECK_EQ(vec[2], 2);
  }

  SUBCASE("begin()/end() const")
  {
    // GIVEN
    constexpr ft::Vector<int, 5> vec{0, 1, 2};

    // WHEN
    int i = 0;
    for (const auto& e : vec) {
      // THEN
      CHECK_EQ(e, i);
      ++i;
    }
    CHECK_EQ(vec.size(), i);
    CHECK_EQ(vec.size(), 3);
  }

  SUBCASE("Random access iterator: std::sort")
  {
    // GIVEN
    ft::Vector<int, 10> vec{9, 4, 7, 1, 5, 6, 3, 8, 0, 2};

    // WHEN
    // clang-format off
    SUBCASE("std::sort") { std::sort(vec.begin(), vec.end()); }
    SUBCASE("std::ranges::sort") { std::ranges::sort(vec); }
    // clang-format on

    // THEN
    for (int i = 0; i < 10; ++i) {
      CHECK_EQ(vec[static_cast<std::size_t>(i)], i);
    }
  }

  SUBCASE("Input iterator: find success")
  {
    // GIVEN
    constexpr ft::Vector<int, 10> vec{9, 4, 7, 1, 5, 6, 3, 8, 0, 2};
    ft::Vector<int, 10>::const_iterator it;
    CHECK_EQ(it.operator->(), nullptr);

    // WHEN
    // clang-format off
    SUBCASE("std::find") { it = std::find(vec.begin(), vec.end(), 3); }
    SUBCASE("std::ranges::find") { it = std::ranges::find(vec, 3); }
    // clang-format on

    // THEN
    CHECK_EQ(it, vec.begin() + 6);
    CHECK_EQ(*it, 3);
    CHECK_EQ(*(it.operator->()), 3);
  }

  SUBCASE("Input iterator: find success")
  {
    // GIVEN
    ft::Vector<int, 10> vec{9, 4, 7, 1, 5, 6, 3, 8, 0, 2};
    ft::Vector<int, 10>::iterator it;
    CHECK_EQ(it.operator->(), nullptr);

    // WHEN
    // clang-format off
    SUBCASE("std::find") { it = std::find(vec.begin(), vec.end(), 3); }
    SUBCASE("std::ranges::find") { it = std::ranges::find(vec, 3); }
    // clang-format on

    // THEN
    CHECK_EQ(it, vec.begin() + 6);
    CHECK_EQ(*it, 3);
    CHECK_EQ(*(it.operator->()), 3);
  }

  SUBCASE("const iterator operator++")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    int expected = 1;

    // WHEN
    for (auto it = vec.cbegin(); it != vec.cend(); it++) {
      // THEN
      CHECK_EQ(*it, expected);
      ++expected;
    }
  }

  SUBCASE("iterator operator++(int)")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    int expected = 1;

    // WHEN
    for (auto it = vec.begin(); it != vec.end(); it++) {
      // THEN
      CHECK_EQ(*it, expected);
      ++expected;
    }
  }

  SUBCASE("const iterator operator--")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    int expected = 3;

    // WHEN
    for (auto it = vec.cend() - 1; it != vec.cbegin(); --it) {
      // THEN
      CHECK_EQ(*it, expected);
      --expected;
    }
  }

  SUBCASE("const iterator operator--(int)")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    int expected = 3;

    // WHEN
    for (auto it = vec.cend() - 1; it != vec.cbegin(); it--) {
      // THEN
      CHECK_EQ(*it, expected);
      --expected;
    }
  }

  SUBCASE("iterator operator--")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    int expected = 3;

    // WHEN
    for (auto it = vec.end() - 1; it != vec.begin(); --it) {
      // THEN
      CHECK_EQ(*it, expected);
      --expected;
    }
  }

  SUBCASE("iterator operator--(int)")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    int expected = 3;

    // WHEN
    for (auto it = vec.end() - 1; it != vec.begin(); it--) {
      // THEN
      CHECK_EQ(*it, expected);
      --expected;
    }
  }

  SUBCASE("const iterator operator+=")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    auto it = vec.cbegin();
    CHECK_EQ(*it, 1);

    // WHEN
    it += 2;

    // THEN
    CHECK_EQ(*it, 3);
  }

  SUBCASE("iterator operator+=")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    auto it = vec.begin();
    CHECK_EQ(*it, 1);

    // WHEN
    it += 2;

    // THEN
    CHECK_EQ(*it, 3);
  }

  SUBCASE("const iterator operator-=")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    auto it = vec.cend() - 1;
    CHECK_EQ(*it, 3);

    // WHEN
    it -= 2;

    // THEN
    CHECK_EQ(*it, 1);
  }

  SUBCASE("iterator operator-=")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    auto it = vec.end() - 1;
    CHECK_EQ(*it, 3);

    // WHEN
    it -= 2;

    // THEN
    CHECK_EQ(*it, 1);
  }

  SUBCASE("const iterator operator+(diff, iter)")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    const auto itBegin = vec.cbegin();

    // WHEN
    const auto it = 2 + itBegin;

    // THEN
    CHECK_EQ(*it, 3);
  }

  SUBCASE("iterator operator+(diff, iter)")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    const auto itBegin = vec.begin();

    // WHEN
    const auto it = 2 + itBegin;

    // THEN
    CHECK_EQ(*it, 3);
  }

  SUBCASE("const iterator operator[]")
  {
    // GIVEN
    constexpr ft::Vector<int, 3> vec{1, 2, 3};
    const auto it = vec.cbegin();

    // WHEN
    const auto value = it[2];

    // THEN
    CHECK_EQ(value, 3);
  }

  SUBCASE("iterator operator[]")
  {
    // GIVEN
    ft::Vector<int, 3> vec{1, 2, 3};
    const auto it = vec.begin();

    // WHEN
    const auto value = it[2];

    // THEN
    CHECK_EQ(value, 3);
  }
}

TEST_CASE("clear")
{
  SUBCASE("T = int")
  {
    // GIVEN
    ft::Vector<int, 5> vec{10, 20, 30, 40};
    CHECK_EQ(vec.size(), 4);

    // WHEN
    vec.clear();

    // THEN
    CHECK_EQ(vec.size(), 0);
  }

  SUBCASE("T = std::string")
  {
    // GIVEN
    ft::Vector<std::string, 5> vec{"aa", "bb", "cc", "dd"};
    CHECK_EQ(vec.size(), 4);

    // WHEN
    vec.clear();

    // THEN
    CHECK_EQ(vec.size(), 0);
  }
}

TEST_CASE("pushBack")
{
  SUBCASE("T = int")
  {
    SUBCASE("pushBack")
    {
      // GIVEN
      ft::Vector<int, 3> vec;
      CHECK_EQ(vec.size(), 0);

      // WHEN
      vec.pushBack(0);
      vec.pushBack(1);
      vec.pushBack(2);

      // THEN
      CHECK_EQ(vec.size(), 3);
      CHECK_EQ(vec[0], 0);
      CHECK_EQ(vec[1], 1);
      CHECK_EQ(vec[2], 2);
    }

    SUBCASE("pushBack after clear")
    {
      // GIVEN
      ft::Vector<int, 3> vec{10, 20};
      CHECK_EQ(vec.size(), 2);

      // WHEN
      vec.clear();
      CHECK_EQ(vec.size(), 0);
      vec.pushBack(42);

      // THEN
      CHECK_EQ(vec.size(), 1);
      CHECK_EQ(vec[0], 42);
    }
  }

  SUBCASE("T = std::string")
  {
    SUBCASE("pushBack")
    {
      // GIVEN
      ft::Vector<std::string, 3> vec;
      CHECK_EQ(vec.size(), 0);

      // WHEN
      vec.pushBack("aa");
      vec.pushBack("bb");
      vec.pushBack("cc");

      // THEN
      CHECK_EQ(vec.size(), 3);
      CHECK_EQ(vec[0], "aa");
      CHECK_EQ(vec[1], "bb");
      CHECK_EQ(vec[2], "cc");
    }

    SUBCASE("pushBack after clear")
    {
      // GIVEN
      ft::Vector<std::string, 3> vec{"aa", "bb"};
      CHECK_EQ(vec.size(), 2);

      // WHEN
      vec.clear();
      CHECK_EQ(vec.size(), 0);
      vec.pushBack("cc");

      // THEN
      CHECK_EQ(vec.size(), 1);
      CHECK_EQ(vec[0], "cc");
    }
  }
}

TEST_CASE("emplaceBack")
{
  SUBCASE("T = int")
  {
    // GIVEN
    ft::Vector<int, 3> vec;
    CHECK_EQ(vec.size(), 0);

    // WHEN
    vec.emplaceBack(42);

    // THEN
    CHECK_EQ(vec.size(), 1);
    CHECK_EQ(vec[0], 42);
  }

  SUBCASE("T = std::string")
  {
    // GIVEN
    ft::Vector<std::string, 3> vec;
    CHECK_EQ(vec.size(), 0);

    // WHEN
    vec.emplaceBack("abcdefg", 2U, 3U);

    // THEN
    CHECK_EQ(vec.size(), 1);
    CHECK_EQ(vec[0], "cde");
  }
}
