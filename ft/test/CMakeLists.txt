ft_add_test(
  NAME file_types_tests
  SOURCES file_types_tests.cpp
  INCLUDES ../src
  LIBRARIES libft
)

ft_add_test(
  NAME vector_tests
  SOURCES vector_tests.cpp
  INCLUDES ../src
)

ft_generate_source(SOURCE libft_tests.cpp)

ft_add_test(
  NAME libft_tests
  SOURCES ${CMAKE_CURRENT_BINARY_DIR}/generated/libft_tests.cpp
  LIBRARIES libft
)

ft_add_test(
  NAME map_tests
  SOURCES map_tests.cpp
  INCLUDES ../src
)

ft_add_test(
  NAME parser_tests
  SOURCES parser_tests.cpp
  INCLUDES ../src
  LIBRARIES libft
)

ft_add_test(
  NAME result_tests
  SOURCES result_tests.cpp
  INCLUDES ../src
)
