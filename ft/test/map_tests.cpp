#include "map.hpp"

#include <doctest/doctest.h>

#include <string_view>

TEST_CASE("Map")
{
  // GIVEN
  using Pair = ft::MapPair<std::string_view, int>;
  constexpr std::array pairs{Pair{"banana", 10}, Pair{"banana2", 42}, Pair{"bapple", -1000}};
  constexpr ft::Map map(pairs);

  // WHEN
  const auto value1 = map.at("banana");

  // THEN
  CHECK(value1);
  CHECK_EQ(*value1, 10);

  // WHEN
  const auto value2 = map.at("banana2");

  // THEN
  CHECK(value2);
  CHECK_EQ(*value2, 42);

  // WHEN
  const auto value3 = map.at("bapple");

  // THEN
  CHECK(value3);
  CHECK_EQ(*value3, -1000);

  // WHEN
  const auto value4 = map.at("not in map");

  // THEN
  CHECK_FALSE(value4);
}
