#include <ft/ft.hpp>

#include <doctest/doctest.h>

#include <filesystem>
#include <iostream>
#include <string_view>
#include <variant>

namespace
{
namespace fs = std::filesystem;
const fs::path TEST_SRC_DIR{"${CMAKE_CURRENT_SOURCE_DIR}"};
const fs::path TEST_DIR{TEST_SRC_DIR / "test_dir"};
}  // namespace

TEST_CASE("Search file contents")
{
  SUBCASE("No pattern")
  {
    // GIVEN
    const ft::Options options;

    // WHEN
    // Searching with an empty pattern string.
    const auto results = ft::search("", options);

    // THEN
    CHECK(results.error);
    CHECK_EQ(results.error->reason, ft::SearchError::Reason::EmptyPattern);
    CHECK_FALSE(results.fileMatches.has_value());
    CHECK_FALSE(results.filenameMatches.has_value());
  }

  SUBCASE("Valid pattern")
  {
    // GIVEN
    const ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching with a valid pattern string.
    const std::string_view pattern{"This is a "};
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect an exact number of matches containing "This is a" in `./test_dir`, equal to the number
    // of test files.
    CHECK(results.fileMatches);
    CHECK_EQ(results.fileMatches->size(), 58);
    // Expect that all matches contain the searched for pattern at the correct position.
    for (const auto& fileMatch : *(results.fileMatches)) {
      for (const auto& match : fileMatch.lineMatches) {
        const auto& matchString = match.stringMatch.string;
        for (const auto& position : match.stringMatch.positions) {
          CHECK_EQ(matchString.substr(position.start, position.end - position.start), pattern);
        }
      }
    }
  }

  SUBCASE("Search a specific file")
  {
    // GIVEN
    const std::string_view pattern{"This is a "};

    // WHEN
    // Searching in a specific file.
    const ft::Options options{.searchPaths{TEST_DIR / "cpp_test" / "test.cpp"}};
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect exactly one match containing "This is a" in `./test_dir/cpp_test/test.cpp`.
    CHECK(results.fileMatches);
    CHECK_EQ(results.fileMatches->size(), 1);
    const auto& match = (*results.fileMatches)[0];
    CHECK(match.filename.ends_with("test.cpp"));
    CHECK_EQ(match.lineMatches.size(), 1);
    CHECK_EQ(match.lineMatches[0].lineNumber, 1);
    const auto& stringMatch = match.lineMatches[0].stringMatch;
    CHECK_EQ(stringMatch.positions.size(), 1);
    const auto& position = stringMatch.positions[0];
    CHECK_EQ(stringMatch.string.substr(position.start, position.end - position.start), pattern);
  }

  SUBCASE("Case sensitive")
  {
    // GIVEN
    const ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching for a pattern that will not match because it has wrong casing.
    const std::string_view pattern{"this is a "};
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect zero matches containing "this is a" in `./test_dir`, since the casing doesn't match.
    CHECK_FALSE(results.fileMatches.has_value());
    CHECK_FALSE(results.filenameMatches.has_value());
  }

  SUBCASE("Case insensitive")
  {
    // GIVEN
    const std::string_view pattern{"this is a "};
    ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching with ignore case set.
    options.ignoreCaseSet = true;
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect an exact number of matches containing "this is a" in `./test_dir`.
    CHECK(results.fileMatches);
    CHECK_EQ(results.fileMatches->size(), 58);
    for (const auto& res : *(results.fileMatches)) {
      CHECK_EQ(res.lineMatches.size(), 1);
      CHECK(res.lineMatches[0].stringMatch.string.starts_with("This is a "));
      CHECK_EQ(res.lineMatches[0].stringMatch.positions.size(), 1);
      CHECK_EQ(res.lineMatches[0].stringMatch.positions[0].start, 0);
      CHECK_EQ(res.lineMatches[0].stringMatch.positions[0].end, 10);
      CHECK_EQ(res.lineMatches[0].lineNumber, 1);
    }
  }

  SUBCASE("Search with filetype include")
  {
    // GIVEN
    const std::string_view pattern{"This is a "};
    ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching for matches in files of a specific type.
    options.includeTypes = {"cpp"};
    const auto results = ft::search(pattern, options);

    // THEN
    CHECK(results.fileMatches);
    CHECK_EQ(results.fileMatches->size(), 9);
  }

  SUBCASE("Search with filetype exclude")
  {
    // GIVEN
    const std::string_view pattern{"This is a "};
    ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching for matches in files EXCEPT for a specific type.
    options.excludeTypes = {"cpp"};
    const auto results = ft::search(pattern, options);

    // THEN
    CHECK(results.fileMatches);
    CHECK_EQ(results.fileMatches->size(), 49);
  }

  SUBCASE("Search with unknown include filetypes")
  {
    // GIVEN
    const std::string_view pattern{"This is a "};
    ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching for matches in unknown filetypes.
    options.includeTypes = {"foobarbaz"};
    const auto results = ft::search(pattern, options);

    // THEN
    CHECK(results.error);
    CHECK_EQ(results.error->reason, ft::SearchError::Reason::UnknownFileTypes);
    CHECK(results.error->info);
    CHECK_EQ(*(results.error->info), "foobarbaz ");
  }

  SUBCASE("Search with unknown exclude filetypes")
  {
    // GIVEN
    const std::string_view pattern{"This is a "};
    ft::Options options{.searchPaths{TEST_DIR}};

    // WHEN
    // Searching for matches in unknown filetypes.
    options.excludeTypes = {"foobarbaz"};
    const auto results = ft::search(pattern, options);

    // THEN
    CHECK(results.error);
    CHECK_EQ(results.error->reason, ft::SearchError::Reason::UnknownFileTypes);
    CHECK(results.error->info);
    CHECK_EQ(*(results.error->info), "foobarbaz ");
  }
}

TEST_CASE("Search filenames")
{
  SUBCASE("No pattern")
  {
    // GIVEN
    const ft::Options options{.searchFilenames{true}};

    // WHEN
    // Searching with an empty pattern string.
    const auto results = ft::search("", options);

    // THEN
    CHECK(results.error);
    CHECK_EQ(results.error->reason, ft::SearchError::Reason::EmptyPattern);
    CHECK_FALSE(results.fileMatches.has_value());
    CHECK_FALSE(results.filenameMatches.has_value());
  }

  SUBCASE("Valid pattern")
  {
    // GIVEN
    const ft::Options options{.searchPaths{TEST_DIR}, .searchFilenames{true}};

    // WHEN
    // Searching with a valid pattern string.
    std::string_view pattern{"test"};
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect an exact number of matches containing "This is a" in `./test_dir`, equal to the number
    // of test files.
    CHECK(results.filenameMatches);
    CHECK_EQ(results.filenameMatches->size(), 80);
    // Expect that all matches contain the searched for pattern at the correct position.
    for (const auto& filenameMatch : *(results.filenameMatches)) {
      const auto& matchString = filenameMatch.string;
      for (const auto& position : filenameMatch.positions) {
        CHECK_EQ(matchString.substr(position.start, position.end - position.start), pattern);
      }
    }
  }

  SUBCASE("Search a specific file")
  {
    // GIVEN
    const std::string_view pattern{"This is a "};
    ft::Options options{.searchFilenames{true}};

    // WHEN
    // Searching in a specific file.
    options.searchPaths = {TEST_DIR / "cpp_test" / "test.cpp"};
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect one match containing an error string.
    CHECK(results.filenameMatches);
    CHECK_EQ(results.filenameMatches->size(), 1);
    // Expect that all matches contain the searched for pattern at the correct position.
    const auto& match = (*results.filenameMatches)[0];
    CHECK_EQ(match.string,
             "[error]: Path '" + options.searchPaths[0].string() + "' is not a directory.");
  }

  SUBCASE("Case sensitive")
  {
    // GIVEN
    const ft::Options options{.searchPaths{TEST_DIR}, .searchFilenames{true}};

    // WHEN
    // Searching for a string that won't match because it has wrong casing.
    const std::string_view pattern{R"(Est\.)"};
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect zero filename matches containing "Est." in `./test_dir`, since the casing doesn't
    // match.
    CHECK_FALSE(results.filenameMatches.has_value());
  }

  SUBCASE("Case insensitive")
  {
    // GIVEN
    const std::string_view pattern{R"(Est\.)"};
    ft::Options options{.searchPaths{TEST_DIR}, .searchFilenames{true}};

    // WHEN
    // Searching with ignore case set.
    options.ignoreCaseSet = true;
    const auto results = ft::search(pattern, options);

    // THEN
    // Expect an exact number of matches containing "this is a" in `./test_dir`.
    CHECK(results.filenameMatches);
    CHECK_EQ(results.filenameMatches->size(), 57);
    for (const auto& res : *(results.filenameMatches)) {
      CHECK_NE(res.string.find("est."), res.string.npos);
      CHECK_EQ(res.positions.size(), 1);
    }
  }

  SUBCASE("Search with filetype include")
  {
    // GIVEN
    const std::string_view pattern{"test"};
    ft::Options options{.searchPaths{TEST_DIR}, .searchFilenames{true}};

    // WHEN
    // Searching for matches in files of a specific type.
    options.includeTypes = {"cpp"};
    const auto results = ft::search(pattern, options);

    // THEN
    CHECK(results.filenameMatches);
    CHECK_EQ(results.filenameMatches->size(), 9);
  }

  SUBCASE("Search with filetype exclude")
  {
    // GIVEN
    const std::string_view pattern{R"(\.cpp)"};
    ft::Options options{.searchPaths{TEST_DIR}, .searchFilenames{true}};

    // WHEN
    // Searching for matches in files EXCEPT for a specific type.
    options.excludeTypes = {"cpp"};
    const auto results = ft::search(pattern, options);

    // THEN
    CHECK_FALSE(results.filenameMatches);
  }
}
