#include "file_types.hpp"

#include <doctest/doctest.h>

#include <filesystem>
#include <string>
#include <unordered_set>

TEST_CASE("getUnknownFileTypes")
{
  // GIVEN
  const std::unordered_set<std::string> includeTypes{"cmake", "CMake", "cpp"};
  const std::unordered_set<std::string> excludeTypes{"elisp", "org", "FindThis"};

  // WHEN
  const auto unknownFileTypes = ft::getUnknownFileTypes(includeTypes, excludeTypes);

  // THEN
  CHECK(unknownFileTypes.has_value());
  CHECK_EQ(unknownFileTypes->size(), 2);
  CHECK(unknownFileTypes->contains("CMake"));
  CHECK(unknownFileTypes->contains("FindThis"));
}

TEST_CASE("searchThisFileType, in include types")
{
  // GIVEN
  std::filesystem::path path;
  std::unordered_set<std::string> includeTypes;
  std::unordered_set<std::string> excludeTypes;
  SUBCASE("Special case: CMakeLists.txt")
  {
    path = "CMakeLists.txt";
    includeTypes = {"cmake"};
  }
  SUBCASE("Special case: *.cmake")
  {
    path = "name.cmake";
    includeTypes = {"cmake"};
  }
  SUBCASE("Normal case")
  {
    path = "source.cpp";
    includeTypes = {"cpp"};
  }
  const auto unknownFileTypes = ft::getUnknownFileTypes(includeTypes, excludeTypes);
  CHECK_FALSE(unknownFileTypes.has_value());

  // WHEN
  const bool searchFile = ft::searchThisFileType(path, includeTypes, excludeTypes);

  // THEN
  CHECK(searchFile);
}

TEST_CASE("searchThisFileType, in exclude types")
{
  // GIVEN
  std::filesystem::path path;
  std::unordered_set<std::string> includeTypes;
  std::unordered_set<std::string> excludeTypes;
  SUBCASE("Special case: CMakeLists.txt")
  {
    path = "CMakeLists.txt";
    excludeTypes = {"cmake"};
  }
  SUBCASE("Special case: *.cmake")
  {
    path = "name.cmake";
    excludeTypes = {"cmake"};
  }
  SUBCASE("Normal case")
  {
    path = "source.cpp";
    excludeTypes = {"cpp"};
  }
  const auto unknownFileTypes = ft::getUnknownFileTypes(includeTypes, excludeTypes);
  CHECK_FALSE(unknownFileTypes.has_value());

  // WHEN
  const bool searchFile = ft::searchThisFileType(path, includeTypes, excludeTypes);

  // THEN
  CHECK_FALSE(searchFile);
}

TEST_CASE("searchThisFileType, include overrides exclude")
{
  // GIVEN
  std::filesystem::path path;
  std::unordered_set<std::string> includeTypes;
  std::unordered_set<std::string> excludeTypes;
  SUBCASE("Special case: CMakeLists.txt")
  {
    path = "CMakeLists.txt";
    includeTypes = {"cmake"};
    excludeTypes = {"cmake"};
  }
  SUBCASE("Special case: *.cmake")
  {
    path = "name.cmake";
    includeTypes = {"cmake"};
    excludeTypes = {"cmake"};
  }
  SUBCASE("Normal case")
  {
    path = "source.cpp";
    includeTypes = {"cpp"};
    excludeTypes = {"cpp"};
  }
  const auto unknownFileTypes = ft::getUnknownFileTypes(includeTypes, excludeTypes);
  CHECK_FALSE(unknownFileTypes.has_value());

  // WHEN
  const bool searchFile = ft::searchThisFileType(path, includeTypes, excludeTypes);

  // THEN
  CHECK(searchFile);
}

TEST_CASE("getFileTypeListString")
{
  // GIVEN, WHEN
  const auto fileTypeList = ft::getFileTypeListString();

  // THEN
  CHECK_NE(fileTypeList.find("bash"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find('c'), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("cmake"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("config"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("cpp"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("csv"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("elisp"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("fish"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("groovy"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("jpeg"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("json"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("lisp"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("markdown"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("org"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("pdf"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("png"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("protobuf"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("python"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("rst"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("sh"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("tar"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("txt"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("vim"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("yaml"), fileTypeList.npos);
  CHECK_NE(fileTypeList.find("zsh"), fileTypeList.npos);
}
