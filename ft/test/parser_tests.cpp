#include "parser.hpp"

#include <doctest/doctest.h>

TEST_CASE("Error")
{
  SUBCASE("No pattern")
  {
    // GIVEN
    const std::vector<std::string> args{};

    // WHEN
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), ft::ParseError::NoPattern);
  }

  SUBCASE("--help")
  {
    // GIVEN
    std::vector<std::string> args{};
    ft::ParseError expectedError{};

    // WHEN
    SUBCASE("-h")
    {
      args.emplace_back("-h");
      expectedError = ft::ParseError::PrintHelp;
    }
    SUBCASE("--help")
    {
      args.emplace_back("--help");
      expectedError = ft::ParseError::PrintHelp;
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), expectedError);
  }

  SUBCASE("--version")
  {
    // GIVEN
    std::vector<std::string> args{};
    ft::ParseError expectedError{};

    // WHEN
    SUBCASE("-V")
    {
      args.emplace_back("-V");
      expectedError = ft::ParseError::PrintVersion;
    }
    SUBCASE("--version")
    {
      args.emplace_back("--version");
      expectedError = ft::ParseError::PrintVersion;
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), expectedError);
  }

  SUBCASE("--type-list")
  {
    // GIVEN
    std::vector<std::string> args{"--type-list"};

    // WHEN
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), ft::ParseError::PrintTypelist);
  }

  SUBCASE("MultipleSameOptionError")
  {
    // GIVEN
    std::vector<std::string> args{"-f", "-f"};

    // WHEN
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), ft::ParseError::MultipleSameOptionError);
  }

  SUBCASE("MissingOptionValueError")
  {
    // GIVEN
    std::vector<std::string> args{"-t"};

    // WHEN
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), ft::ParseError::MissingOptionValueError);
  }

  SUBCASE("Unknown option")
  {
    // GIVEN
    std::vector<std::string> args{};

    // WHEN
    SUBCASE("Misspelled --help")
    {
      args.emplace_back("--halp");
    }
    SUBCASE("Misspelled --version")
    {
      args.emplace_back("--versin");
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK_FALSE(result);
    CHECK_EQ(result.error(), ft::ParseError::UnknownOption);
  }
}

TEST_CASE("Success")
{
  SUBCASE("Valid pattern")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};

    // WHEN
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK_EQ(result->pattern, args[0]);
  }

  SUBCASE("Specify search paths")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};
    std::vector<std::string> paths;

    // WHEN
    SUBCASE("One path")
    {
      paths = {"path/i/want/to/search"};
      args.insert(args.end(), paths.begin(), paths.end());
    }
    SUBCASE("Multiple paths")
    {
      paths = {"path/i/want/to/search",
               "./a/relative/path",
               "/an/absolut/path",
               "a/path/to/an/explicit/directory/"};
      args.insert(args.end(), paths.begin(), paths.end());
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK_EQ(result->options.searchPaths.size(), paths.size());
    for (int i = 0; i < paths.size(); ++i) {
      CHECK_EQ(result->options.searchPaths[i], paths[i]);
    }
  }

  SUBCASE("Search in filenames")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};

    // WHEN
    SUBCASE("Long option")
    {
      args.emplace_back("--filename");
    }
    SUBCASE("Short option")
    {
      args.emplace_back("-f");
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK(result->options.searchFilenames);
  }

  SUBCASE("Ignore case")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};

    // WHEN
    SUBCASE("Long option")
    {
      args.emplace_back("--ignore-case");
    }
    SUBCASE("Short option")
    {
      args.emplace_back("-i");
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK(result->options.ignoreCaseSet);
  }

  SUBCASE("Case sensitive")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};

    // WHEN
    SUBCASE("Long option")
    {
      args.emplace_back("--case-sensitive");
    }
    SUBCASE("Short option")
    {
      args.emplace_back("-s");
    }
    SUBCASE("Overrides --ignore-case before")
    {
      args.insert(args.end(), {"-s", "-i"});
    }
    SUBCASE("Overrides --ignore-case after")
    {
      args.insert(args.end(), {"-i", "-s"});
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK(result->options.caseSensitiveSet);
  }

  SUBCASE("Include types in search")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};

    // WHEN
    SUBCASE("Long option")
    {
      args.insert(args.end(), {"--type", "cpp", "--type", "cmake"});
    }
    SUBCASE("Short option")
    {
      args.insert(args.end(), {"-t", "cpp", "-t", "cmake"});
    }
    SUBCASE("Mixed long/short options")
    {
      args.insert(args.end(), {"--type", "cpp", "-t", "cmake"});
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK_EQ(result->options.includeTypes.size(), 2);
    CHECK(result->options.includeTypes.contains("cpp"));
    CHECK(result->options.includeTypes.contains("cmake"));
  }

  SUBCASE("Exclude types in search")
  {
    // GIVEN
    std::vector<std::string> args{"foobar"};

    // WHEN
    SUBCASE("Long option")
    {
      args.insert(args.end(), {"--not-type", "cpp", "--not-type", "cmake"});
    }
    SUBCASE("Short option")
    {
      args.insert(args.end(), {"-T", "cpp", "-T", "cmake"});
    }
    SUBCASE("Mixed long/short options")
    {
      args.insert(args.end(), {"--not-type", "cpp", "-T", "cmake"});
    }
    const auto result = ft::parse(args);

    // THEN
    CHECK(result);
    CHECK_EQ(result->options.excludeTypes.size(), 2);
    CHECK(result->options.excludeTypes.contains("cpp"));
    CHECK(result->options.excludeTypes.contains("cmake"));
  }
}
