#include "result.hpp"

#include <doctest/doctest.h>

#include <string>

namespace ft
{
namespace
{
enum struct TrivialError
{
  Reason,
};

template<typename R, typename E>
Result<R, E> getResult(const R& r)
{
  return r;
}

template<typename R, typename E>
Result<R, E> getResult(const E& e)
{
  return e;
}
}  // namespace

TEST_CASE("Get a valid result")
{
  // GIVEN
  const int r = 10;

  // WHEN
  auto res = getResult<int, TrivialError>(r);
  const auto constRes = res;

  // THEN
  CHECK(res.hasResult());
  CHECK(res);
  CHECK_EQ(*res, r);
  CHECK_EQ(*constRes, r);
}

TEST_CASE("Get an error")
{
  // GIVEN
  const TrivialError e = TrivialError::Reason;

  // WHEN
  auto res = getResult<int, TrivialError>(e);
  const auto& constRes = res;

  // THEN
  CHECK_FALSE(res.hasResult());
  CHECK_FALSE(res);
  CHECK_EQ(res.error(), e);
  CHECK_EQ(constRes.error(), e);
}
}  // namespace ft
