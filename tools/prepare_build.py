#!/usr/bin/env python3

import argparse
import shutil
import subprocess
from pathlib import Path

CMAKE = shutil.which("cmake")
assert CMAKE, "Could not find CMake in PATH"

BUILD_TYPE = {
    "Debug": "Debug",
    "FastDebug": "RelWithDebInfo",
    "Release": "Release",
    "Coverage": "Coverage", # Helper build type for generating code coverage.
}

# This is a non-exhaustive listing of compilers added for fast translation from what I
# usually say to what I usually mean in terms of compilers.
COMPILER = {
    "gcc": "g++",
    "clang": "clang++",
    "msvc": "cl",
}


def parse_args():
    parser = argparse.ArgumentParser(
        description="Prepare a build of FindThis",
    )
    parser.add_argument(
        "--build-path",
        "-p",
        type=str,
        help="path to directory in which to put build directories",
        required=True,
    )
    parser.add_argument(
        "--build-types",
        "-t",
        type=str,
        nargs="+",
        choices=BUILD_TYPE.keys(),
        help="which build types to prepare, default to Release",
        default=["Release"],
    )
    parser.add_argument(
        "--compilers",
        "-c",
        type=str,
        nargs="*",
        help="name of compiler executable to use, defaults to let CMake find a native compiler",
        default=["native"],
    )
    parser.add_argument(
        "--generator",
        "-G",
        type=str,
        help="build generator, defaults to Ninja",
        default="Ninja",
    )
    return parser.parse_args()


def prepare_build(build_path, compilers, build_types, generator):
    path_to_root_dir = Path(__file__).resolve().parent.parent
    for compiler in compilers:
        for build_type in build_types:
            build_dir = Path(build_path) / compiler / build_type
            if build_dir.exists():
                # Make sure to remove any old files before creating new ones.
                shutil.rmtree(build_dir)

            command = [
                CMAKE,
                f"-DCMAKE_BUILD_TYPE={BUILD_TYPE[build_type]}",
                "-G",
                generator,
                "-B",
                str(build_dir),
                "-S",
                str(path_to_root_dir),
            ]
            if compiler != "native":
                cxx = COMPILER.get(compiler) or compiler
                command.append(f"-DCMAKE_CXX_COMPILER={cxx}")

            with subprocess.Popen(
                command,
                bufsize=1,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                text=True,
            ) as p:
                for line in p.stdout:
                    print(line, end="")


def main():
    args = parse_args()
    prepare_build(**vars(args))


if __name__ == "__main__":
    main()
